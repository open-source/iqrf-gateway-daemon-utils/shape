cmake_minimum_required(VERSION 3.18)

project(shape LANGUAGES CXX C)

set(CMAKE_C_STANDARD 11)
set(CMAKE_C_EXTENSIONS ON)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_EXTENSIONS ON)

set(CMAKE_POSITION_INDEPENDENT_CODE ON)

set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/lib)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

set(PROJECT_CMAKE_MODULE_PATH ${PROJECT_CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/cmake")
set(${PROJECT_NAME}_CMAKE_MODULE_PATH ${PROJECT_CMAKE_MODULE_PATH})
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${PROJECT_SOURCE_DIR}/cmake")

include(${PROJECT_SOURCE_DIR}/cmake/ShapeComponentDeclaration.cmake)
include(${PROJECT_SOURCE_DIR}/cmake/ccache.cmake)
include(${PROJECT_SOURCE_DIR}/cmake/hardening.cmake)

set_directory_properties(PROPERTIES COMPILE_DEFINITIONS $<$<CONFIG:Debug>:_DEBUG>)

if(${SHAPE_STATIC_LIBS})
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -DSHAPE_STATIC_LIBS")
endif()

message(STATUS "CMAKE_CXX_STANDARD:      ${CMAKE_CXX_STANDARD}")
message(STATUS "CMAKE_CXX_FLAGS:         ${CMAKE_CXX_FLAGS}")
message(STATUS "CMAKE_CXX_FLAGS_RELEASE: ${CMAKE_CXX_FLAGS_RELEASE}")
message(STATUS "CMAKE_CXX_FLAGS_DEBUG:   ${CMAKE_CXX_FLAGS_DEBUG}")
message(STATUS "CMAKE_C_FLAGS:           ${CMAKE_C_FLAGS}")
message(STATUS "CMAKE_C_FLAGS_RELEASE:   ${CMAKE_C_FLAGS_RELEASE}")
message(STATUS "CMAKE_C_FLAGS_DEBUG:     ${CMAKE_C_FLAGS_DEBUG}")

include_directories(${CMAKE_BINARY_DIR})
include_directories(${CMAKE_SOURCE_DIR}/include)

# Configure config file.
# This file specifies actions performed and variables exported when using find_package on this project.
# The find_package requires properly set ${PROJECT_NAME}_DIR variable to a location where the
# configured config file persists. This variable is usually specified from the outside of CMake.
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/${PROJECT_NAME}Config.cmake.in ${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake @ONLY)

# Description and default value of options
option(BUILD_TESTING "Builds the test." NO)
option(SHAPE_BUILD_EXAMPLES "Builds the shape's examples." NO)
option(SHAPE_DEPLOY "Shape deployment path.")
option(SHAPE_STATIC_LIBS "Builds shape as static library." NO)

#apply just configured file
include(${PROJECT_BINARY_DIR}/${PROJECT_NAME}Config.cmake)

set(PROJECT_INSTALL_PREFIX ${PROJECT_NAME})
set(CMAKE_INSTALL_PREFIX ${shape_DEPLOY})

add_definitions(-DRAPIDJSON_HAS_STDSTRING)

#library build
add_subdirectory(include)
add_subdirectory(startup)
add_subdirectory(launcher)
add_subdirectory(TraceFormatService)
add_subdirectory(TraceFileService)

if(${SHAPE_BUILD_EXAMPLES})
	add_subdirectory(examples/Example1_Thread)
endif()

if(${BUILD_TESTING})
	#external submodules
	# Build google test as shared library
	set(BUILD_SHARED_LIBS OFF CACHE BOOL "" FORCE)
	#add_definitions(-DBUILD_SHARED_LIBS=1)
	# Prevent overriding the parent project's compiler/linker settings on Windows
	set(gtest_force_shared_crt ON CACHE BOOL "" FORCE)
	add_subdirectory(external/googletest)
	#add_subdirectory(GTestRunner)
	add_subdirectory(GTestStaticRunner)
endif()
