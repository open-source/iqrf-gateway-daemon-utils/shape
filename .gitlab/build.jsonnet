local distributions = import 'build/distributions.jsonnet';
local options = import 'build/options.jsonnet';

// Docker image suffix
local imageSuffix(distribution, arch=null) = distribution.name + '-' + distribution.version + if arch != null then if arch != 'all' then '-' + arch else '' else if distribution.arch != 'all' then '-' + distribution.arch else '';

// Machine architecture
local machineArch(arch) = if arch == 'all' then 'amd64' else arch;

// Docker image
local image(distribution) = {
	image: options.baseImage + ':' + imageSuffix(distribution, machineArch(distribution.arch)),
	variables: {
		ARCH: distribution.arch,
		DIST: distribution.version,
	} + if distribution.name == 'raspbian' then {
		ARCH_PREFIX: 'rpi-',
		CFLAGS: '-marm -march=armv6zk -mcpu=arm1176jzf-s -mfloat-abi=hard -mfpu=vfp',
		CXXFLAGS: '-marm -march=armv6zk -mcpu=arm1176jzf-s -mfloat-abi=hard -mfpu=vfp',
	} else {},
	tags: ['linux', machineArch(distribution.arch)],
};

// Build job definition
local buildJob(distribution) = {
	stage: 'build',
	before_script: [
		'git checkout -B "$CI_COMMIT_REF_NAME" "$CI_COMMIT_SHA"',
		'git submodule init',
		'git submodule update',
	] + if options.ccache then [
		'export CCACHE_COMPILERCHECK="content"',
		'export CCACHE_COMPRESS="true"',
		'export CCACHE_BASEDIR="$PWD"',
		'export CCACHE_DIR="${CCACHE_BASEDIR}/.ccache"',
		'ccache --zero-stats --show-stats',
		'echo "CCACHEDIR=~/.ccache" > ~/.pbuilderrc',
	],
	script: [
		'cmake -Bbuild -H. -DCMAKE_SKIP_INSTALL_RPATH=TRUE -DCMAKE_SKIP_BUILD_RPATH=TRUE -DCMAKE_SKIP_RPATH=TRUE -DCMAKE_BUILD_TYPE=Debug -DUSE_CCACHE=TRUE',
		'cmake --build build',
	],
} + image(distribution) + (
	if options.ccache then {
		after_script:
			[
				'export CCACHE_DIR="${PWD}/.ccache"',
				'ccache --show-stats',
			],
		cache:
			{
				key: '$CI_JOB_NAME',
				paths: ['.ccache/'],
			},
	} else {}
);

{
	['build' + '/' + imageSuffix(distribution)]: buildJob(distribution)
	for distribution in distributions
}
